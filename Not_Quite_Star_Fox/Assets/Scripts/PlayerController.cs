﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerController : MonoBehaviour
{
    [Header("General")]
    [Tooltip("In ms^-1")][SerializeField]float ControlSpeed = 25f;
    [Tooltip("In ms^-1")] [SerializeField] float xRange = 18f;
    [Tooltip("In ms^-1")] [SerializeField] float yRangeD = 12f;
    [Tooltip("In ms^-1")] [SerializeField] float yRangeU = 8f;
    [SerializeField] GameObject[] bullets;

    [Header("Screen-position Based")]
    [SerializeField] float positionPitchFactor = -2f;
    [SerializeField] float positionYawFactor = 2f;

    [Header("Control-throw Based")]
    [SerializeField] float controlPitchFactor = -4f;
    [SerializeField] float controlRollFactor = 4f;

    float xThrow, yThrow;
    bool movementEnabled = true; 

    // Update is called once per frame
    void Update()
    {
        if (movementEnabled)
        { 
        HorizontalMovement();
        VeritcalMovement();
        ProcessRotation();
        ProcessFiring();
        }
    }

    private void StopMovement()
    {
        movementEnabled = false;
    }

    private void ProcessRotation()
    {
        float pitchDueToPosition = transform.localPosition.y * positionPitchFactor;
        float pitchDueToControlThrow = yThrow * controlPitchFactor;
        float pitch = pitchDueToPosition + pitchDueToControlThrow;


        float yaw = transform.localPosition.x * positionYawFactor;


        float roll = xThrow * controlRollFactor;

        transform.localRotation = Quaternion.Euler(pitch, yaw, roll);
    }

    private void HorizontalMovement()
    {
        xThrow = CrossPlatformInputManager.GetAxis("Horizontal");
        float xOffset = xThrow * ControlSpeed * Time.deltaTime;
        float rawXPos = transform.localPosition.x + xOffset;
        float clampedXPos = Mathf.Clamp(rawXPos, -xRange, xRange);
        transform.localPosition = new Vector3(clampedXPos,
            transform.localPosition.y,
            transform.localPosition.z);
    }

    private void VeritcalMovement()
    {
        yThrow = CrossPlatformInputManager.GetAxis("Vertical");
        float yOffset = yThrow * ControlSpeed * Time.deltaTime;
        float rawYPos = transform.localPosition.y + yOffset;
        float clampedYPos = Mathf.Clamp(rawYPos, -yRangeD, yRangeU);
        transform.localPosition = new Vector3(transform.localPosition.x,
    clampedYPos,
    transform.localPosition.z);
    }

    private void ProcessFiring()
    {
        if (CrossPlatformInputManager.GetButton("Fire"))
        {
            ActivateBullets();
        }
        else
        {
            DeactivateBullets();
        }
    }

    private void ActivateBullets()
    {
        foreach (GameObject bullet in bullets)
        {
            bullet.SetActive(true);
        }
    }

    private void DeactivateBullets()
    {
        foreach (GameObject bullet in bullets)
        {
            bullet.SetActive(false);
        }
    }
}
