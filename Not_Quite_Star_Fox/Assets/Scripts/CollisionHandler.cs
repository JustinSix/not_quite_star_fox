﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CollisionHandler : MonoBehaviour
{
    [Tooltip("In seconds.")][SerializeField] float levelLoadDelay = 0.6f;

    [Tooltip("FX prefab on player")][SerializeField] GameObject deathFX;

    private void OnTriggerEnter(Collider other)
    {
        StartDeathSequence();
    }

    private void StartDeathSequence()
    {
        SendMessage("StopMovement");
        PlayExplosion();
        Invoke("ReloadScene", levelLoadDelay);
    }



    private void PlayExplosion()
    {
        deathFX.SetActive(true);
    }

    private void ReloadScene()
    {
        SceneManager.LoadScene(1);
    }
}
